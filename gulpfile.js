
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var npmDist = require('gulp-npm-dist');
var fileinclude = require('gulp-file-include');
var del = require('del');

gulp.task('sass-css', function () {
  return gulp.src('scss/tangdao.scss')
    .pipe(sass())
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('minify-css', function () {
  return gulp.src('scss/tangdao.scss')
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// styles for pages
gulp.task('sass-pages', function () {
  return gulp.src('scss/pages/*.scss')
    .pipe(sass())
    .pipe(rename({ prefix: 'tangdao.' }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// styles for skins
gulp.task('sass-skins', function () {
  return gulp.src('scss/skins/*.scss')
    .pipe(sass())
    .pipe(rename({ prefix: 'skin.' }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browserSync', function () {
  browserSync.init({
    server: true
  })
});

// Copy dependencies to lib/
gulp.task('npm-lib', function () {
  gulp.src(npmDist(), { base: './node_modules/' })
    .pipe(rename(function (path) {
      path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '');
    }))
    .pipe(gulp.dest('lib'));
});

gulp.task('dist1', function() {
  gulp.src(['./src/components/*.html'])//主文件
      .pipe(fileinclude({
          prefix: '@@',//变量前缀 @@include
          basepath: './src/_include',//引用文件路径
          indent:true//保留文件的缩进
      }))
      .pipe(gulp.dest('components'));//输出文件路径
});

gulp.task('dist2', function() {
  gulp.src(['./src/collections/*.html'])//主文件
      .pipe(fileinclude({
          prefix: '@@',//变量前缀 @@include
          basepath: './src/_include',//引用文件路径
          indent:true//保留文件的缩进
      }))
      .pipe(gulp.dest('collections'));//输出文件路径
});

gulp.task('clean', function () {
  return del.sync(['components', 'collections']);
});

//编译模板 parallel 并行，series 串行
// gulp.task('dist', gulp.parallel("dist1","dist2","dist3"));
gulp.task('dist', gulp.series('sass-css', 'sass-pages', gulp.parallel("dist1","dist2")));

gulp.task('default', gulp.series('browserSync', function () {
  gulp.watch([
    'scss/bootstrap/*.scss',
    'scss/tangdao/*.scss',
    'scss/fonts/*.scss',
    'scss/lib/*.scss',
    'scss/panels/*.scss',
    'scss/util/*.scss',
    'scss/_mixins.scss',
    'scss/tangdao.scss'
  ], gulp.series('sass-css'));

  gulp.watch('scss/pages/*.scss', gulp.series('sass-pages'));
  gulp.watch('scss/skins/*.scss', gulp.series('sass-skins'));

  gulp.watch([
    'scss/_variables.scss',
    'scss/bootstrap/_variables.scss'
  ], gulp.series(gulp.parallel('sass-css', 'sass-pages')));

  gulp.watch('index.html', browserSync.reload);
  gulp.watch('templates/**/*.html', browserSync.reload);
  gulp.watch('components/**/*.html', browserSync.reload);
  gulp.watch('collections/**/*.html', browserSync.reload);
  gulp.watch('assets/js/*.js', browserSync.reload);
}));