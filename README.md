# Tangdao Design
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitee.com/ruyangit/tangdao-design/blob/master/LICENSE)  [![star](https://gitee.com/ruyangit/tangdao-design/badge/star.svg?theme=dark)](https://gitee.com/ruyangit/tangdao-design/stargazers)    

#### 介绍
Tangdao Design 是一个优质的现代干净的Bootstrap 4中台模板，使用简单和最小的设计。它使用SASS预处理器、HTML5、CSS3和jQuery插件完全响应。它使用了大多数helper/utilities类，这些类是可重用的类，可以加快页面的速度并提高页面时间加载的速度。它可以用于大多数类型的应用程序模板，如分析、管理等程序，也可以用于您的桌面或移动应用程序。惊人的灵活性和可重用性。

#### 组件示例
<img src="doc/introduction.png" width="100%" />
<img src="doc/modal.png" width="100%" />

#### 文档说明
<a href="http://ruyangit.gitee.io/tangdao-design/index.html">预览</a> 

#### 安装教程
```
安装
$ npm install -g gulp-cli   安装gulp客户端
$ gulp --version            查看版本
$ npm install sass          安装sass
$ sass --version            查看版本
$ npm install               安装插件
```

```
打包
$ gulp npm-lib              更新插件
$ gulp sass-css             编译核心样式
$ gulp sass-pages           编译页面样式
$ gulp sass-skins           编译主题库
$ gulp minify-css           压缩样式（可以不执行）
```

```
运行
$ gulp default              开发运行
```

#### 使用说明
```
Stylesheet
<!-- vendor css -->
<link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

<!-- Tangdao CSS -->
<link rel="stylesheet" href="../assets/css/tangdao.css">
<!-- some additional css here -->
```
```
Scripts
<script src="../lib/jquery/jquery.min.js"></script>
<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../lib/feather-icons/feather.min.js"></script>

<script src="../assets/js/tangdao.js"></script>
```
```
Dashboard
<header class="navbar navbar-header navbar-header-fixed">
  <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
  <div class="navbar-brand">
    <a href="../index.html" class="td-logo">dash<span>forge</span></a>
  </div>
  <div id="navbarMenu" class="navbar-menu-wrapper">
    <div class="navbar-menu-header">
      <a href="../index.html" class="td-logo">dash<span>forge</span></a>
      <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
    </div>
    <ul class="nav navbar-menu">...</ul>
  </div>
  <div class="navbar-right">...</div>
</header>
<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    ...
  </div>
</div>
<footer class="footer">...</footer>
```
```
Apps
<header class="navbar navbar-header navbar-header-fixed">...</header>

<!-- app-wrapper is a custom class that you can add in the style (ie. contact-wrapper, chat-wrapper)-->
<!-- check app pages for references -->
<div class="app-wrapper">...</div>
```
```
Pages
<header class="navbar navbar-header navbar-header-fixed">...</header>

<div class="content content-fixed bd-b">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    ...
  </div>
</div>

<footer class="footer">...</footer>
```

#### References

<table class="table table-docs">
    <thead>
    <tr>
        <th class="wd-30p">Plugins</th>
        <th class="wd-70p">Link</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Bootstrap</td>
        <td><a href="https://getbootstrap.com/">https://getbootstrap.com/</a></td>
    </tr>
    <tr>
        <td>Chart JS</td>
        <td><a href="http://www.chartjs.org/">http://www.chartjs.org/</a></td>
    </tr>
    <tr>
        <td>CryptoFont</td>
        <td><a href="http://cryptofont.com">@AMPoellmann</a></td>
    </tr>
    <tr>
        <td>DataTables</td>
        <td><a href="https://datatables.net/">https://datatables.net/</a></td>
    </tr>
    <tr>
        <td>Flag Icon CSS</td>
        <td><a href="https://github.com/lipis/flag-icon-css">https://github.com/lipis/flag-icon-css</a></td>
    </tr>
    <tr>
        <td>Flot</td>
        <td><a href="http://www.flotcharts.org/">http://www.flotcharts.org/</a></td>
    </tr>
    <tr>
        <td>Flot CurvedLines</td>
        <td><a href="http://curvedlines.michaelzinsmaier.de/">Michael Zinsmaier and nergal.dev</a></td>
    </tr>
    <tr>
        <td>FontAwesome</td>
        <td><a href="http://fontawesome.io/">http://fontawesome.io/</a></td>
    </tr>
    <tr>
        <td>FullCalendar</td>
        <td><a href="https://fullcalendar.io/">https://fullcalendar.io/</a></td>
    </tr>
    <tr>
        <td>GMaps</td>
        <td><a href="https://hpneo.github.io/gmaps/">https://hpneo.github.io/gmaps/</a></td>
    </tr>
    <tr>
        <td>IonRangeSlider</td>
        <td><a href="https://ionden.com">IonDen.com</a></td>
    </tr>
    <tr>
        <td>Ionicons</td>
        <td><a href="http://ionicons.com/">http://ionicons.com/</a></td>
    </tr>
    <tr>
        <td>jQuery</td>
        <td><a href="https://jquery.com/">https://jquery.com/</a></td>
    </tr>
    <tr>
        <td>jQuery Sparkline</td>
        <td><a href="http://digitalbush.com/projects/masked-input-plugin/">http://digitalbush.com/projects/masked-input-plugin/</a></td>
    </tr>
    <tr>
        <td>jQuery Steps</td>
        <td><a href="http://www.jquery-steps.com/">http://www.jquery-steps.com/</a></td>
    </tr>
    <tr>
        <td>jQuery UI</td>
        <td><a href="https://jqueryui.com/">https://jqueryui.com/</a></td>
    </tr>
    <tr>
        <td>jQuery MaskedInput</td>
        <td><a href="https://digitalbush.com/projects/masked-input-plugin/">https://digitalbush.com/projects/masked-input-plugin/</a></td>
    </tr>
    <tr>
        <td>jQVMap</td>
        <td><a href="http://jqvmap.com">http://jqvmap.com</a></td>
    </tr>
    <tr>
        <td>Leaflet</td>
        <td><a href="http://leafletjs.com">http://leafletjs.com</a></td>
    </tr>
    <tr>
        <td>LightSlider</td>
        <td><a href="https://github.com/sachinchoolur/lightslider">https://github.com/sachinchoolur/lightslider</a></td>
    </tr>
    <tr>
        <td>LineAwesome</td>
        <td><a href="https://icons8.com/line-awesome">https://icons8.com/line-awesome</a></td>
    </tr>
    <tr>
        <td>Moment JS</td>
        <td><a href="https://momentjs.com/">https://momentjs.com/</a></td>
    </tr>
    <tr>
        <td>Morris JS</td>
        <td><a href="http://morrisjs.github.io/morris.js/">http://morrisjs.github.io/morris.js/</a></td>
    </tr>
    <tr>
        <td>Parsley JS</td>
        <td><a href="http://parsleyjs.org">http://parsleyjs.org</a></td>
    </tr>
    <tr>
        <td>Peity</td>
        <td><a href="http://benpickles.github.io/peity">http://benpickles.github.io/peity</a></td>
    </tr>
    <tr>
        <td>Perfect Scrollbar</td>
        <td><a href="http://utatti.github.io/perfect-scrollbar/">http://utatti.github.io/perfect-scrollbar/</a></td>
    </tr>
    <tr>
        <td>Popper JS</td>
        <td><a href="https://popper.js.org/">https://popper.js.org/</a></td>
    </tr>
    <tr>
        <td>Quill</td>
        <td><a href="https://quilljs.com/">https://quilljs.com/</a></td>
    </tr>
    <tr>
        <td>Raphael</td>
        <td><a href="http://dmitrybaranovskiy.github.io/raphael/">http://dmitrybaranovskiy.github.io/raphael/</a></td>
    </tr>
    <tr>
        <td>Select2</td>
        <td><a href="https://select2.org/">https://select2.org/</a></td>
    </tr>
    <tr>
        <td>Spectrum</td>
        <td><a href="https://bgrins.github.io/spectrum/">https://bgrins.github.io/spectrum/</a></td>
    </tr>
    <tr>
        <td>Typicons</td>
        <td><a href="https://www.s-ings.com/typicons/">https://www.s-ings.com/typicons/</a></td>
    </tr>
    </tbody>
</table>
