# tangdao-design

#### Description
Tangdao 是一个优质的现代干净的Bootstrap 4中台模板，使用简单和最小的设计。它使用SASS预处理器、HTML5、CSS3和jQuery插件完全响应。它使用了大多数helper/utilities类，这些类是可重用的类，可以加快页面的速度并提高页面时间加载的速度。它可以用于大多数类型的应用程序模板，如分析、管理等程序，也可以用于您的桌面或移动应用程序。惊人的灵活性和可重用性。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
